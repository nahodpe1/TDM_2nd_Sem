using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FillPercentUpdater : MonoBehaviour
{
    [SerializeField] private float speedMultiplier = 1;
    [SerializeField] private float sinusSpeedMultiplier = 1;

    [Space]
    [SerializeField] private float minValue;
    [SerializeField] private float maxValue;
    [SerializeField] private float currentValue;

    [Space]
    [SerializeField] private float direction;
    public float Direction { get => direction; set => direction = value; }

    [Space]
    [SerializeField] private Material targetMaterial;
    [SerializeField] private string targetMaterialPropertyName;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
    }

    void Update()
    {
        float directionSign = MathF.Sign(direction);

        float sin = MathF.Sin(Time.time * sinusSpeedMultiplier);
        float delta = directionSign * Time.deltaTime * (sin + 0.4f) * speedMultiplier;

        currentValue = Math.Clamp(currentValue + delta, minValue, maxValue);

        targetMaterial.SetFloat(targetMaterialPropertyName, currentValue);
    }
}
