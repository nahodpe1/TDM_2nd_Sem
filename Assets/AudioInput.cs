using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class AudioInput : MonoBehaviour
{
    [SerializeField] private float tooLowNoiseLevel = 0.1f;
    [SerializeField] private float highEnoughNoiseLevel = 0.3f;
    [SerializeField] private FillPercentUpdater fillPercentUpdater;



    private CancellationTokenSource cts;

    private async void OnEnable()
    {
        cts = new CancellationTokenSource();
        try
        {
            await ClipRecording(cts.Token);
        }
        catch (Exception ex)
        {
            if(ex is not OperationCanceledException)
            {
                Debug.LogException(ex);
            }
        }
    }

    private void OnDisable()
    {
        cts.Cancel();
    }

    private async Task ClipRecording(CancellationToken token)
    {
        float[] samplesArray = Array.Empty<float>();

        while (true)
        {
            var clip = Microphone.Start(
                deviceName: "",
                loop: false,
                lengthSec: 1,
                frequency: 44_000);



            try
            {
                await Task.Delay(1000, token);
                if(clip.samples > samplesArray.Length)
                {
                    Array.Resize(ref samplesArray, clip.samples);
                }
                clip.GetData(samplesArray, 0);
            }
            finally
            {
                Destroy(clip);
            }

            float noiseLevel = 
                await Task.Run(() => 
                    samplesArray
                    .Select(f => MathF.Abs(f))
                    .Average());

            float direction;

            if (noiseLevel < tooLowNoiseLevel)
            {
                direction = -1;
            }
            else if (noiseLevel > highEnoughNoiseLevel)
            {
                direction = 1;
            }
            else direction = 0;

            Debug.Log($"noise level {noiseLevel}, direction of tree lights {direction}");

            fillPercentUpdater.Direction = direction;
        }
    }
}
